package designpatterns.strutural.decorator;

public class EggKottu extends KottuDecorator {
	public EggKottu(Kottu customkottu) {
		super (customkottu);
		
	}
private String addEgg() {
	return " + Egg";
	
}
public String make() {
	return CustomKottu.make()+addEgg();
}
}
