package designpatterns.strutural.decorator;

public class KottuDecoratorDemo {
	public static void main(String args []) {
	Kottu NormalKottu = new NormalKottu();
	System.out.println(NormalKottu.make());
	
	Kottu eggKottu = new EggKottu(new NormalKottu());
	System.out.println(eggKottu.make());
	
	Kottu meatKottu = new MeatKottu(new EggKottu(new NormalKottu()));
	System.out.println(meatKottu.make());
	
}
}
