package designpatterns.strutural.decorator;

public interface Kottu {
   public String make();
}
