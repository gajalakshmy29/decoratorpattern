package designpatterns.strutural.decorator;

public enum Meat {
    chicken,
    Beef,
    fork,
    mutton,
    seafood
}
