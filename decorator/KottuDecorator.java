package designpatterns.strutural.decorator;

public class KottuDecorator implements Kottu {
protected Kottu CustomKottu;
public KottuDecorator(Kottu customeKottu) {
	this.CustomKottu = customeKottu;
	
}
public String make() {
	return CustomKottu.make();
	
}
}
