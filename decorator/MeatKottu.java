package designpatterns.strutural.decorator;

public class MeatKottu  extends KottuDecorator {
	public MeatKottu(Kottu customeKottu) {
		super(customeKottu);
		// TODO Auto-generated constructor stub
	}
	
private String addMeat() {
	return " + Meat";
	
}
public String make() {
	return CustomKottu.make()+addMeat();
}
}